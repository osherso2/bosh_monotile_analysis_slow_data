import os, pandas, numpy, matplotlib.pyplot as plt
g = "/home/osherso2/scratch/glitch_libs"
h = "lib_f00_v10"

cut_version = 0

keys = {}
libs = {}
cuts = {}
strs = {}
libf = os.path.join(g, h+"_c%02d" % cut_version)
strf = os.path.join(g, h+"_c%02d_SR" % cut_version)

def apply_cut(lib, cut, status_report, update_sr=False, reverb=False):

    cuts = []
    for k, m in lib.iterrows(): cuts.append(cut in m["cut"])
    exc = numpy.array(cuts)
    inc = numpy.logical_not(exc)

    nlib = lib[inc].copy()
    acut = lib[exc].copy()
    if update_sr: status_report = update_livetime(status_report, acut, reverb=reverb) 

    return nlib, acut, status_report.copy()

def update_livetime(status_report, cut, reverb=False):

    dets = numpy.unique(cut["det"])
    for det in dets:

        ind = cut["det"] == det
        sel = status_report["det"] == det
        N = status_report.loc[sel, "N"].iloc[0]
        
        if reverb:
            new_flag = status_report.loc[sel, "reverb"].iloc[0]
        else:
            new_flag = numpy.zeros(N).astype("bool")
            for i, m in cut[ind].iterrows(): new_flag[i-10:i+15] = True

        new_flag = numpy.logical_or(new_flag, status_report.loc[sel, "flag"].iloc[0])
        status_report.loc[sel, "live time"] = N - len(new_flag[new_flag])
        status_report.at[numpy.where(sel)[0][0], "flag"] = new_flag.copy()

    return status_report

######################################################################################################

olib = pandas.read_pickle(libf)

lib = olib.copy()
status_report = pandas.read_pickle(strf)

c =-1
print (c, "steps")
keys[c] = "steps"
libs[c] = lib[lib["step"]].copy()
cuts[c] = None
strs[c] = status_report.copy() 

c+= 1
lib, exc, status_report = apply_cut(lib, "step", status_report, update_sr=False)
print (c, "all glitches") 
keys[c] = "all glitches"
libs[c] = lib
cuts[c] = None
strs[c] = status_report.copy() 

c+= 1
lib, exc, status_report = apply_cut(lib, "pathological detector", status_report, update_sr=False)
print (c, "pathology cut") 
keys[c] = "pathology cut"
libs[c] = lib
cuts[c] = exc
strs[c] = status_report.copy() 

c+= 1
lib, exc, status_report = apply_cut(lib, "low S/N", status_report, update_sr=False)
print (c, "S/N cut") 
keys[c] = "S/N cut"
libs[c] = lib
cuts[c] = exc
strs[c] = status_report.copy() 

c+= 1
lib, exc, status_report = apply_cut(lib, "low detector std", status_report, update_sr=False)
print (c, "det std cut") 
keys[c] = "det std cut"
libs[c] = lib
cuts[c] = exc
strs[c] = status_report.copy() 

c+= 1
lib, exc, status_report = apply_cut(lib, "within pre-existing flags", status_report, update_sr=True)
print (c, "flag cut") 
keys[c] = "flag cut"
libs[c] = lib
cuts[c] = exc
strs[c] = status_report.copy() 

c+= 1
lib, exc, status_report = apply_cut(lib, "negative integral energy", status_report, update_sr=False)
print (c, "negative integral energy cut") 
keys[c] = "negative integral energy cut"
libs[c] = lib
cuts[c] = exc
strs[c] = status_report.copy() 

c+= 1
lib, exc, status_report = apply_cut(lib, "template energy threshhold", status_report, update_sr=False)
print (c, "template energy threshhold") 
keys[c] = "template energy threshhold"
libs[c] = lib
cuts[c] = exc
strs[c] = status_report.copy() 

c+= 1
lib, exc, status_report = apply_cut(lib, "reverb", status_report, update_sr=True, reverb=True)
print (c, "reverb cut") 
keys[c] = "reverb cut"
libs[c] = lib
cuts[c] = exc
strs[c] = status_report.copy() 

c+= 1
lib, exc, status_report = apply_cut(lib, "close to evn", status_report, update_sr=True)
print (c, "evn overlap") 
keys[c] = "evn overlap"
libs[c] = lib
cuts[c] = exc
strs[c] = status_report.copy() 

c+= 1
lib, exc, status_report = apply_cut(lib, "index fuckup", status_report, update_sr=False)
print (c, "template failure") 
keys[c] = "template failure"
libs[c] = lib
cuts[c] = exc
strs[c] = status_report.copy() 

rate = 50e6 / 100. / 33. / 120.

def counter(status_report, lib):

    counts = {}
    live_times = {}
    dead_list = {}
    for r in range(33): 
        for c in [4, 5, 6, 7]:
            det = "r%02dc%02d" % (r, c)
            sel = status_report["det"] == det
            dead = bool(status_report.loc[sel, "dead"].iloc[0])
            dead_list[det] = dead
            if dead: 
                counts[det] = 0
                live_times[det] = 0
                continue
            N_events = len(lib[lib["det"] == det])
            counts[det] = N_events
            live_times[det] = float(status_report.loc[sel, "live time"].iloc[0])/rate


    return counts, live_times, dead_list

cmap = plt.get_cmap("gnuplot")
mark = ["-^", "-*", "-P", "-o", "-X", "-D", "-h", "-8", "-v"]
if __name__ == "__main__":

    xticks = [[], []]

    counts = {}
    live_times = {}
    for r in range(33):
        for c in [4, 5, 6, 7]:
            det = "r%02dc%02d" % (r, c)
            counts[det] = [[], []]
            live_times[det] = [[], []]

    for key in keys.keys():

        if key == -1: continue

        xticks[0].append(key)
        xticks[1].append(keys[key])

        label = keys[key]
        lib = libs[key]
        srp = strs[key]
       
        cnt, ltm, ddl = counter(srp, lib)
       
        for i, det in enumerate(numpy.unique(lib["det"])):
            if ddl[det]: continue
            counts[det][0].append(key)
            counts[det][1].append(cnt[det])
            live_times[det][0].append(key)
            live_times[det][1].append(ltm[det])

    for r in range(33):
        for c in [4, 5, 6, 7]:
            det = "r%02dc%02d" % (r, c)
            det_ind = r + 33.*(c - 4)
            cr = cmap(det_ind/132.)

            plt.subplot(211)
            kl, cnt = counts[det]
            plt.plot(kl, cnt, '-o', color=cr, alpha=.5)

            plt.subplot(212)
            ml, ltm = live_times[det]
            plt.plot(kl, ltm, '-o', color=cr, alpha=.5)
            
    plt.subplot(211)
    plt.ylabel("count")
    plt.xticks(xticks[0], xticks[1], rotation='vertical')

    plt.subplot(212)
    plt.xlabel("key")
    plt.ylabel("live times")
    plt.xticks(xticks[0], xticks[1], rotation='vertical')

    plt.show()            
        

