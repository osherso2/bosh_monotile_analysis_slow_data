from TOD import *
import pandas
nan = numpy.nan
"""
Author: bosh
Last edit: 20190903

Take in data (one step better than raw) from the TOD objects and completely clean / find glitches and steps. 
"""
class FNDR(TOD):
 
    glch_locs = None
    step_locs = None

    glch_lib = None
    step_lib = None

    def __init__(self):

        TOD.__init__(self)

        decimate = self.decimate; rate = self.rate
      
        g_template = self.g_template_long - self.g_template_long[0]
        g_template_fit = self.glch_fit(g_template, glch_rate=rate*decimate, shift=0)
        self.g_template_fit = dict(g_template_fit)
        self.g_template_s += g_template_fit["template index"] - numpy.argmin(g_template)
        self.g_template_s = -self.g_template_s 

        s_template = self.s_template_long
        s_template_fit = self.step_fit(s_template, step_rate=rate*decimate, shift=0)
        self.s_template_fit = dict(s_template_fit) 
        self.s_template_s = numpy.argmax(s_template) - s_template_fit["template index"] - 6

    def find_glitches(self, cuts=[20., 15., 10., 5., 5.], ms_ws=10000):

        assert(not self.clean is None)

        N = self.N
        flag = self.flag
        sf_ws= (5, 10)

        self.flag_hist = []
        self.locs_hist = []
        squeaky = self.clean.copy(); squeaky[flag] = nan
        squeaky-= median_stream(squeaky, ws=ms_ws, flag=flag)

        glch_locs = numpy.array([])
        for i, cut in enumerate(cuts):

            glch, locs = spike_finder(squeaky, cut=cut, ws=sf_ws)
            if len(locs) == 0: continue
            if len(glch_locs) > 0: locs = locs[numpy.vectorize(lambda x: x not in glch_locs)(locs)]
             
            self.flag_hist.append(flag.copy())
            self.locs_hist.append(locs.copy())
            flag = numpy.logical_or(flag, glch)
            glch_locs = numpy.sort(numpy.append(locs, glch_locs)).astype("int")

            squeaky = self.clean.copy(); squeaky[flag] = nan
            squeaky-= median_stream(squeaky, ws=ms_ws, flag=flag)
            for g in locs: squeaky[max(0, g-5):min(g+10, N)] = 0 

        self.flag = flag
        self.glch_locs = glch_locs

        squeaky = self.clean.copy(); squeaky[flag] = nan
        stream = median_stream(squeaky, ws=10000, flag=flag)
        squeaky-= stream

        self.squeaky = squeaky.copy()
        self.medianstream = stream.copy()

        masked = squeaky.copy()
        masked[flag] = nan
        stdstream = nan_std_stream(masked, ws=100)
        stdstream = median_stream(stdstream, ws=2000)
        self.stdstream = stdstream

    def plot_glch(self, params, onx=True):

        j = params["jndex"] + 10 
        i = params["index"]
        o = params["offset"]
        a = params["amplitude"]
        ei = params["integral energy"]
        et = params["template energy"]
        glch = params["data"]
        flag = params["flag"]
        dec = self.decimate

        gfit = generate_glch(len(glch), index=j, size=a, offset=o, decimate=dec)
        x = numpy.arange(len(glch))
        if onx: x - j + i

        fig = plt.figure()
        plt.plot(x, glch, color='b', label=r"$E_i = %.02e eV$" % ei)
        plt.plot(x, gfit, label=r"$E_t = %.02e eV$" % et)
        plot_flag(flag, x=x)

        plt.xlabel("index")
        plt.ylabel("ADU")
        plt.xlim((x[0], x[-1]))

        plt.legend(loc="lower right")

    def plot_step(self, params, onx=True):

        j = params["jndex"] + 10
        i = params["index"]
        o = params["offset"]
        a = params["amplitude"]
        step = params["data"]
        flag = params["flag"]
        dec = self.decimate

        gfit = generate_step(len(glch), index=j, size=a, offset=o, decimate=dec)
        x = numpy.arange(len(glch))
        if onx: x - j + i

        fig = plt.figure()
        plt.plot(x, step, color='b')
        plt.plot(x, gfit)
        plot_flag(flag, x=x)

        plt.xlabel("index")
        plt.ylabel("ADU")
        plt.xlim((x[0], x[-1]))

        plt.legend(loc="lower right")

    def plotter(self, params, onx=True):

        if params["step"]: self.plot_step(params, onx=onx)
        else: self.plot_glch(params, onx=onx)

    def glch_fit(self, glch, shift=None, glch_rate=None, plot=False):

        g_template = self.g_template
        template_rate = self.rate * self.decimate

        if shift is None: shift = self.g_template_s
        if glch_rate is None: glch_rate = self.rate

        decimate = int(float(template_rate) / float(glch_rate))

        glch_fine = numpy.repeat(glch, decimate) 
        glch_fine[numpy.arange(len(glch_fine))%decimate != 0] = 0

        conv = numpy.convolve(glch_fine, g_template, mode='same')
        mc = numpy.argmax(conv)
        a = conv[mc]

        i = round((mc + shift)/decimate)
        o = (mc + shift - i*decimate)

        new_g = generate_glch(len(glch), index=i, size=1., offset=o, decimate=decimate)
        a /= numpy.sum(new_g**2.)
        et = self.g_template_e*abs(a)
        ei = self.integral_estimator(glch)

        X = numpy.sum(numpy.square(glch - new_g))

        if plot:
            gen_fine = generate_glch(len(glch_fine), index=mc+shift, size=a, offset=0, decimate=1)
            plt.plot(glch_fine, c='k', alpha=.5)
            plt.plot(gen_fine, c='r', alpha=.5, label=r"fit $\chi^2 = %.02e$" % X)
            plt.axvline(mc+shift, color='r')
            plt.title(r"$E_i = %.02e eV$, $E_t = %.02e eV$" % (ei, et))
            plt.legend()

        res = {}
        res["data"] = glch
        res["file"] = self.f 
        res["det"] = None
        res["det ind"] = nan
        res["template index"] = i 
        res["index"] = i 
        res["offset"] = o 
        res["amplitude"] = a
        res["peak depth"] = numpy.nanmin(glch)
        res["template energy"] = et
        res["integral energy"] = ei
        res["chi squared"] = X
        res["flag"] = nan
        res["index fuckup"] = nan
        res["step"] = False
        res["cut"] = "" 
        res["normalized chisq"] = X/(a**2.)

        return res 

    def step_fit(self, step, step_rate=None, shift=None, plot=False):

        step -= (step[0] + step[-1])/2.

        s_template = self.s_template
        s_template_rate = self.rate * self.decimate

        if shift is None: shift = self.s_template_s
        if step_rate is None: step_rate = self.rate

        decimate = int(float(s_template_rate) / float(step_rate))

        step_fine = numpy.repeat(step, decimate) 
        step_fine[numpy.arange(len(step_fine))%decimate != 0] = 0

        conv = numpy.convolve(step_fine, s_template, mode='same')
        mc = numpy.argmax(conv)

        i = int(round((mc + shift)/decimate))
        o = int(mc + shift - i*decimate)
        a = numpy.percentile(step[i+1:], 75) - numpy.percentile(step[:i-1], 25)

        new_s = generate_step(len(step), index=i, offset=o, decimate=decimate)
        X = numpy.sum(numpy.square(step - new_s))

        if plot:
            gen_fine = generate_step(len(step_fine), index=int(mc+shift), offset=0, decimate=1)
            gen_fine-= (gen_fine[-1] - gen_fine[0])/2.
            plt.plot(step_fine, c='k', alpha=.5)
            plt.plot(gen_fine, c='r', alpha=.5, label=r"fit $\chi^2 = %.02e$" % X)
            plt.axvline(mc+shift, color='r')
            plt.legend()

        res = {}
        res["data"] = step 
        res["file"] = self.f 
        res["det"] = None
        res["det ind"] = nan
        res["template index"] = i - 6 
        res["index"] = i - 6 
        res["offset"] = o 
        res["amplitude"] = a 
        res["peak depth"] = nan 
        res["template energy"] = nan 
        res["integral energy"] = nan 
        res["chi squared"] = X
        res["flag"] = nan
        res["index fuckup"] = nan
        res["step"] = True 
        res["cut"] = "" 
        res["normalized chisq"] = X/(a**2.)

        return res 

    def glch_analysis(self, glch_locs, curr_flag):

        assert(not glch_locs is None)
        if len(glch_locs) < 1: return            

        if not self.det is None: 
            det = "r%02dc%02d" % self.det
            det_ind = self.det[0] + 33.*(self.det[1] - 4)
        else: 
            det = ""
            det_ind = nan

        window = [30, 30]
        subset = [10, 15]
        clean = self.clean.copy()
        flag = curr_flag.copy()

        glch_lib = []
        for q, i in enumerate(glch_locs): 
           
            if i < window[0] or i > self.N-window[1]: continue 
            if numpy.any(flag[i-subset[0]:i+subset[1]]): continue
            data = clean[i-window[0]:i+window[1]]
            glch = clean[i-subset[0]:i+subset[1]]
            glch-= self.medianstream[i-subset[0]:i+subset[1]]

            param = self.glch_fit(glch); 
            j = param["template index"]
            ind = int(j - subset[0] + i)

            cut = ""
            if numpy.any(flag[i-subset[0]:i+subset[1]]): cut += ":within pre-existing flags" 

            gstd = numpy.nanmedian(self.stdstream)
            lstd = self.stdstream[ind]
            excs = numpy.sum(glch < -3.*lstd)
            if numpy.isnan(lstd): std = gstd
            else: std = lstd

            param["det"] = det
            param["det ind"] = det_ind
            param["data"] = data.copy()
            param["flag"] = flag[i-window[0]:i+window[1]].copy()
            param["index fuckup"] = int(j - subset[0])
            param["index"] = i
            param["template index"] = ind
            param["local std"] = lstd
            param["width"] = excs
            param["cut"] = cut 
            param["signal to noise"] = abs(param["peak depth"]/std)
            glch_lib.append(dict(param))

        glch_lib = pandas.DataFrame(glch_lib)
        self.glch_lib = glch_lib
        return glch_lib

    def step_analysis(self):

        step_locs = self.step_locs
        assert(not step_locs is None)
        if len(step_locs) < 1: return            

        if not self.det is None: 
            det = "r%02dc%02d" % self.det
            det_ind = self.det[0] + 33.*(self.det[1] - 4)
        else: 
            det = ""
            det_ind = nan

        window = [30, 30]
        subset = [10, 15]
        data = self.data.copy()
        artifacts = self.flx.copy()

        step_lib = []
        for k in step_locs: 
            i = int(round(k))
            if i < window[0] or i > self.N-window[1]: continue 
            dats = data[i-window[0]:i+window[1]]
            flag = artifacts[i-window[0]:i+window[1]]
            step = data[i-subset[0]:i+subset[1]] 

            param = self.step_fit(step); 
            j = param["template index"] - 6
            ind = j - window[0] + i

            lstd = self.stdstream[ind]

            param["det"] = det 
            param["det ind"] = det_ind
            param["data"] = dats.copy() 
            param["flag"] = flag.copy() 
            param["index fuckup"] = j - subset[0] 
            param["index"] = i
            param["template index"] = ind 
            param["local std"] = lstd 
            param["width"] = nan
            param["cut"] = "" 
            param["signal to noise"] = param["amplitude"]/lstd
            step_lib.append(dict(param))

        step_lib = pandas.DataFrame(step_lib)
        self.step_lib = step_lib
        return step_lib

    def make_lib(self):
        
        step_lib = self.step_analysis()

        glch_lib = None
        for glch_loc, curr_flag in zip(self.locs_hist, self.flag_hist):
            try: glch_nlib = self.glch_analysis(glch_loc, curr_flag)
            except: continue 

            if not isinstance(glch_nlib, pandas.DataFrame): continue
            if len(glch_nlib) == 0: continue

            if glch_lib is None: glch_lib = glch_nlib
            else: glch_lib = glch_lib.append(glch_nlib, sort=False).reset_index(drop=True)

        if glch_lib is None: raise RuntimeError("glch_analysis failed - possible dead detector.")
        if step_lib is None: lib = glch_lib
        else: lib = glch_lib.append(step_lib, sort=False).reset_index(drop=True)

        ### Filling in more details ###

        flx = self.flx
        x = numpy.arange(len(flx))
        flx_inds = x[flx] 

        def nearest(ind, inds):
            ind = int(ind)
            if len(inds) == 0: return nan, nan
            nearness = inds - ind
            try: nearness_f = min(nearness[nearness >= 0])
            except: nearness_f = nan    
            try: nearness_b = max(nearness[nearness <= 0])
            except: nearness_b = nan
            return nearness_b, nearness_f
        def nearest_arr(lib, inds, exclude_self=False):
            nears_b = []; nears_f = []
            for k, m in lib.iterrows():
                k = m["index"]
                if exclude_self: nb, nf = nearest(k, inds[inds!=k])
                else: nb, nf = nearest(k, inds) 
                nears_b.append(nb)
                nears_f.append(nf)
            return nears_b, nears_f                
        
        flx_b, flx_f = nearest_arr(lib, flx_inds, exclude_self=False)
        flx_n = numpy.nanmin([numpy.abs(flx_b), flx_f], axis=0)
        lib.insert(0, "prox to prev flx", flx_b)
        lib.insert(0, "prox to next flx", flx_f)
        lib.insert(0, "prox to near flx", flx_n)
        
        glch_b, glch_f = nearest_arr(lib, lib["index"], exclude_self=True)
        glch_n = numpy.nanmin([numpy.abs(glch_b), glch_f], axis=0)
        lib.insert(0, "prox to prev glch", glch_b)
        lib.insert(0, "prox to next glch", glch_f)
        lib.insert(0, "prox to near glch", glch_n)

        evn_b = numpy.nanmax([flx_b, glch_b], axis=0)
        evn_f = numpy.nanmin([flx_f, glch_f], axis=0)
        evn_n = numpy.nanmin([numpy.abs(evn_b), evn_f], axis=0)
        lib.insert(0, "prox to prev evn", evn_b)
        lib.insert(0, "prox to next evn", evn_f)
        lib.insert(0, "prox to near evn", evn_n)

        self.lib = lib
        return lib

    def hyper_stack(self, lib=None, plot=False, test_on_template=False, decimate=5):

        # Check inputs
        if lib is None: lib = self.glch_lib
    
        window=(20, 25)
        ws = window[0] + window[1]
        data = self.clean - self.medianstream; # Detrended, de-stepped data
        decimate = int_check(decimate) 
        
        stack = numpy.zeros(ws*decimate) 
        wayts = numpy.zeros(ws*decimate) # Get it? Wieghts? Number of points per 'bin' in stack
        wayt = numpy.ones(ws*decimate)  
        wayt[numpy.arange(ws*decimate)%decimate!= 0] = 0
        for i, o, a, et, ei, X in glch_lib:

            i = int_check(i); o = int_check(o)
            if i < window[0] or i > self.N - window[1]: continue
            # Grab the glitch. Compute the new offset (p) in the new decimation rate.
            glch = data[i-window[0] :i+window[1]]; p = int(round(o*float(decimate)/self.decimate))

            if test_on_template:
                # Option to test this function on generated glitches.
                glch = generate_glch(ws, index=window[0] , size=a, offset=o, decimate=self.decimate)
            if plot:
                x = numpy.arange(ws) - o/float(self.decimate)
                plt.plot(x, glch/a, 'o', alpha=.25)

            # Put glitch on a finer array. All zeros except where the downsampled data array was. 
            glch_fine = numpy.repeat(glch, decimate)
            glch_fine[numpy.arange(len(glch_fine))%decimate != 0] = 0
            glch_fine /= a # Normalize by fit amplitude.
            # Rolling to enact the offset into the right place.
            glch_fine = numpy.roll(glch_fine, -p)

            if numpy.any(numpy.isnan(glch_fine)): continue 

            if plot: 
                glch_plot = glch_fine.copy()
                glch_plot[glch_plot == 0] = nan
                x = numpy.arange(ws*decimate)/float(decimate) 
                plt.plot(x, glch_plot, 'o', alpha=.25)

            stack += glch_fine
            # Rolled / enacted the same offset. 
            # So this sum keeps track of to which points this glitch contributed. 
            wayts += numpy.roll(wayt, -p) 

        # I don't want a bunch of nans or infs.
        nonzeros = wayts > 0
        res = stack.copy(); res[nonzeros] /= wayts[nonzeros]

        if plot:
            x = numpy.arange(ws*decimate)/float(decimate) 
            plt.plot(x, res, color='k')
            plt.ylim((-2, 1))
            dec_frac = self.decimate/decimate
            ideal = generate_glch(ws*decimate, index=window[0] *decimate, size=1, offset=0, decimate=dec_frac)
            plt.plot(x, ideal, c='r', lw=3, alpha=.5)

        return res, stack, wayts
