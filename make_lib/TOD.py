from TOD_lib import *

"""
Author: bosh
Last edit: 20190903

Code to read in raw, turn it into data (one step of processing).
Also provide methods for faking data.
"""

class TOD_base:

    f = f 

    bias = 3000. 
    R0 = .7*32e-3

    raw = None
    fld = None
    flx = None 
    fjf = None
    N = numpy.nan

    def __init__(self, det=None, 
                       mux_rate=50000000., row_len=100., num_rows=33., decimate=120.,# Multiplexer 
                       bias=3000, R0=.7*32e-3, Rb=1096., Rsh=3e-3,                   # Bias circuit
                       bias_bits=16., bias_maxV=2.5,                                 # More bias circuit
                       fb_R=15200., fb_bits=14., fb_maxV=.965, m=8.,                 # Feedback circuit
                       Lg=20.):                                                      # Loopgain

        # Mulitplexer
        self.mux_rate = mux_rate
        self.row_len = row_len
        self.num_rows = num_rows
        self.decimate = decimate
        self.rate = mux_rate / row_len / num_rows / decimate

        # Bias circuits
        self.bias = bias
        self.R0 = R0
        self.Rb = Rb
        self.Rsh = Rsh
        self.bias_bits = bias_bits
        self.bias_maxV = bias_maxV

        # Feedback circuit
        self.fb_R = fb_R
        self.fb_bits = fb_bits
        self.fb_maxV = fb_maxV
        self.m = m
        
        # Detector physics 
        self.Lg = Lg

        # Unit conversion tools (ADU -> Amps)   
        self.fb_A = lambda val: feedback_ADU_to_A(val, fb_bits=fb_bits, fb_maxV=fb_maxV, fb_R=fb_R, m=m)
        self.b_A  = lambda val: bias_ADU_to_A(val, bias_bits=bias_bits, bias_maxV=bias_maxV, bias_R=Rb)

        try:
            _, self.det = read_det(det)
        except: self.det = None            

        # Necessary to create glitch template to discover amplitude/energy ratio. 
        n = 250 + (decimate - (250 % decimate))
        g_template_t = numpy.arange(2000)/(self.rate * decimate)
        g_template = generate_glch(l=2000, index=int(n), size=1, offset=0, decimate=1)
        self.g_template_s = numpy.argmin(g_template) - n
        self.g_template_e = self.integral_estimator(g_template, t=g_template_t)

        g_template -= g_template[0] + numpy.trapz(g_template)/2000. # Centering
        self.g_template_long_t = g_template_t.copy()
        self.g_template_long = g_template.copy()
        
        g_template_t = g_template_t[:1250]
        g_template = g_template[:1250]
        
        g_template -= g_template[0] + numpy.trapz(g_template)/1250. # Centering
        self.g_template_t = g_template_t.copy()
        self.g_template = g_template.copy()

        # If I'm going to make a glitch template, I'll make a step template too.
        self.s_template_s = n + 125
        s_template_t = self.g_template_long_t.copy() 
        s_template = generate_step(l=2000, index=int(n+125), offset=0, decimate=1)
        
        s_template -= s_template[0] + numpy.trapz(s_template)/2000. # Centering
        self.s_template_long_t = s_template_t.copy()
        self.s_template_long = s_template.copy()
        
        s_template_t = s_template_t[:1250]
        s_template = s_template[:1250]
        
        s_template -= s_template[0] + numpy.trapz(s_template)/1250. # Centering
        self.s_template_t = s_template_t.copy()
        self.s_template = s_template.copy()

    def integral_estimator(self, glitch, t=None):
        
        rate = self.rate
        bias = self.bias
        
        fb_A = self.fb_A
        b_A = self.b_A
    
        Rb = self.Rb
        Rsh = self.Rsh
        Lg = 20 
        R0 = self.R0

        d = glitch.copy()
        if t is None: t = numpy.arange(len(d))/rate

        d = fb_A(d)
        Vb = b_A(bias) * Rb

        return get_energy(t, d, Vb, Rsh, Rb, R0, Lg)

    def inject_glitches(self, glitches=numpy.array([100.]*20)):

        assert(not self.raw is None)

        g_template_e = self.g_template_e

        raw = self.raw
        decimate = self.decimate
        N = self.N

        injected_glitches = []
        for e in glitches:
            size   = e / self.g_template_e
            offset = numpy.random.randint(241) - 120
            index  = numpy.random.randint(N-26)
            if numpy.isnan(e):
                glch = generate_step(N, index=index, offset=offset, decimate=decimate)
                index = numpy.argmin(numpy.abs(glch - numpy.nanmedian(glch)))
            else:
                glch = generate_glch(N, index=index, size=size, offset=offset, decimate=decimate)
                index = numpy.argmin(glch)
            raw += glch

            injected_glitches.append((index, size, offset, e))

        injected_glitches = numpy.array(injected_glitches)
        self.injected_glitches = injected_glitches
        self.raw = raw 

        return injected_glitches

class fakeTOD(TOD_base):

    def fake_noise(self, N=300000, bias=None, R0=None, spectrum=noise_spec):

        decimate = self.decimate
        rate = self.rate

        self.N = N
        if bias is None:
            bias = self.bias
        else: 
            self.bias = bias
        if R0 is None:
            R0 = self.R0
        else:
            self.R0 = R0

        self.g_template_e = self.integral_estimator(self.g_template - self.g_template[0], t=self.g_template_t)

        if spectrum is None: raw = numpy.zeros(N)
        else:
            noise = ng.noise(spectra=spectrum)
            raw = noise.random_data(N=N+10, fsamp=rate)[1]
            raw = raw[:N]

        flx = numpy.zeros(len(raw)).astype('bool')
        fld = numpy.zeros(len(raw)).astype('bool')
        fjf = numpy.zeros(len(raw)).astype('bool')

        self.bias = bias

        self.N = N
        self.raw = raw 
        self.fld = fld
        self.flx = flx
        self.fjf = fjf
        
        return raw

    def inject_glitch_spectrum(self, N_glitches=1000, step_thresh=1e5,
            particle_spec=lambda N: inv_particle(N, 'g4_island.npy')):

        raw = self.raw
        decimate = self.decimate
        N = self.N

        ens = particle_spec(int(N_glitches))
        ens[ens > step_thresh] = numpy.nan

        injected_glitches = self.inject_glitches(ens) 
        self.injected_glitches = injected_glitches
        self.raw = raw 

        return injected_glitches

    def fold_and_flx(self):

        raw = self.raw
        decimate = self.decimate
        rate = self.rate
        N = self.N

        j = 0
        ix = 1
        top = 2**28/2044.
        usr = numpy.zeros(N)
        curr = rate*120.
        while curr*ix < N:

            i = int((ix-1)*curr)
            j = int(ix*curr)
            raw[i:j] = ((raw[i:j] + top/2.) %  top) - top/2.

            val = raw[j]
            raw[j:] -= val
            usr[j:] += 1
            curr += rate*120.
        raw[j:] = ((raw[j:] + top/2.) %  top) - top/2.

        usr = numpy.append(numpy.diff(usr), 0).astype('bool')
        fld = unfold_flag(raw)
        fjf = numpy.zeros(len(raw)).astype('bool')
        flx = grow_flags(usr, 5, one_way=False)
        flx = grow_flags(flx, 10, one_way=True)

        self.raw = raw 
        self.usr = usr
        self.fld = fld
        self.flx = flx
        self.fjf = fjf

class realTOD(TOD_base):

    def load_dirfile(self, f=f, det=None):
        """
        Read in a dirfile. Create the easy flags.
        """

        if det is None: det = self.det
        assert(not det is None)
        _, det = read_det(det)
        self.det = det

        self.f = f
        self.bias = get_bias(f, col=det[1])

        raw = get_filt(f, self.det)
        fld = unfold_flag(raw)
        usr = flx_lp_init_flag(f).astype("bool")
        fjf = fj_flag(f, self.det)
        flx = grow_flags(usr,  5, one_way=False)
        flx = grow_flags(flx, 10, one_way=True)

        N = len(raw)
        self.N = N 
        self.raw = raw
        self.usr = usr
        self.fld = fld
        self.flx = flx
        self.fjf = fjf

class TOD(TOD_base, realTOD, fakeTOD):

    data = None
    flag = None

    def __init__(self, **kwargs):

        TOD_base.__init__(self, **kwargs)

    def raw_to_data(self, plot=False):

        assert(not self.raw is None)

        raw = self.raw
        fld = self.fld
        flx = self.flx
        fjf = self.fjf

        raw -= raw[0]
        data = unfold(raw, fld)
        data = stitch_steps(data, flx)
        data-= data[0]

        flag = flx.copy() 

        self.data = data
        self.flag = flag

        if plot:
            
            fig = plt.figure()

            ax1 = plt.subplot(211)
            plt.plot(raw)
            plot_flag(flag, lsd={'alpha':.25, 'color':(0,0,1)})
            plt.xlim(0, len(raw))

            ax2 = plt.subplot(212, sharex=ax1)
            plt.plot(data)
            plot_flag(flag, lsd={'alpha':.25, 'color':(0,0,1)})
            plt.xlim(0, len(data))

            plt.show()
            
    def data_to_clean(self, plot=False, ms_ws=100):

        assert(not self.data is None)

        N = self.N
        data = self.data
        flag = self.flag

        steps, step_locs = step_finder(data, cut=-50000, decimate=self.decimate)
        flag = numpy.logical_or(steps, flag)
        clean = stitch_steps(data, flag)
        if ms_ws > 0: clean-= median_stream(clean, ws=ms_ws)

        self.flag = flag
        self.clean = clean

        self.step_locs = step_locs

        if plot:
            
            fig = plt.figure()

            ax1 = plt.subplot(211)
            plt.plot(data)
            plot_flag(flag, lsd={'alpha':.25, 'color':(0,0,1)})
            plt.xlim(0, len(data))

            ax2 = plt.subplot(212, sharex=ax1)
            plt.plot(clean)
            plot_flag(flag, lsd={'alpha':.25, 'color':(0,0,1)})
            plt.xlim(0, len(clean))

    def golden_chunk(self, data=None, ws=100):

        assert(not self.flag is None)
        if data is None:
            if not self.clean is None: data = self.clean
            elif not self.data is None: data = self.data
            elif not self.raw is None: data = self.raw
            else: raise ValueError("Load in data before using golden_chunk.")
                
        chunk = cleanest_chunk(data, self.flag, ws=ws)
        x = numpy.arange(chunk[0], chunk[1])
        y = data[x]
        m = numpy.polyfit(x, y, 1)
        y -= numpy.polyval(m, x)
        return x, data[x], y
