import numpy, os, re
import pygetdata as gd
import matplotlib.pyplot as plt
from scipy.signal import argrelmax, argrelmin
import useful_filters as uf
import noise_generator as ng
from energy_estimators import get_energy
from adu_conversion import feedback_ADU_to_A
from adu_conversion import bias_ADU_to_A

"""
Author: bosh
Last edit: 20190903

All the helper functions to read and processes data.
"""

# The data we used to work this code out.
f = "/projects/physics/filippini/RUN19/20171012"
f = os.path.join(f, "run19_126Hz_DM10_SQsRebiased_allcols_20171012")

def int_check(x): 
    assert(int(x) == x)
    return int(x)

# Be able to accept det naming as either (example) "r04c06" or (4, 6).
def read_det(det):
    
    if (type(det) == str):
        g = re.search("r[0-9][0-9]c[0-9][0-9]", det)
        if not g is None:
            try: det = g.group(0)
            except: raise ValueError("Bad det passed to read_det.") 
            return det, (int(det[1:3]), int(det[4:6]))
        raise ValueError("Bad det passed to read_det.") 
    if (type(det) in (tuple, list, numpy.ndarray)):
        assert(len(det) == 2)
        return "r%02dc%02d" % det, tuple(det)
    raise ValueError("Bad det passed to read_det.") 

### EXTRACTING EASY DATA ###

# Pull a detector's data out of 
def get_filt(f, det):
    
    det, _ = read_det(det) 

    field = "filt_" + det

    f = gd.dirfile(f)
    data = f.getdata(field) / 2044.

    return data

# Find flux jump flag (may help in finding steps?)
def fj_flag(f, det):
    
    f = gd.dirfile(f)
    flag = f.getdata('fj_r%02dc%02d' % det)
    return numpy.append(numpy.diff(flag), 1).astype('bool')

# Recover our flx_lp_init flagging.
def flx_lp_init_flag(f):
    
    f = gd.dirfile(f)
    flag = f.getdata('userfield')
    return numpy.append(numpy.diff(flag), 0).astype('bool')

# Given a file and a col, return the bias of that column.
def get_bias(f, col=0):
    
    if f[-1] == '/': g = f[:-1]+".run"
    else: h = f+".run"

    h = open(h, 'r')
    t = h.read()
    h.close()

    p = re.compile("<RB tes bias> .*")
    m = p.findall(t)[0][14:]
    m = numpy.array(m.split(" ")).astype("int")
    return m[col]

# Find when data loops over.
def unfold_flag(data):
    
    diff = numpy.append(numpy.diff(data), 0)
    fold = abs(diff) > 120000
    return fold 

# Un over/underflow the data based on the unfold_flag's flags.
def unfold(data, fold_flag):
        
    clean = data.copy()
    diff = numpy.diff(clean)
    fold = numpy.where(fold_flag)[0]
    for ix in fold:
       if diff[ix] > 0: clean[ix+1:] -= 2**28/2044.
       if diff[ix]<= 0: clean[ix+1:] += 2**28/2044.

    return clean

### MANIPULATING FLAGS ###

def plot_flag(flag, x=None, lsd={'alpha':.25, 'color':(1,0,0)}, label=None):
    """
    Given flag array, plots a shaded region (default (1,0,0,.25))
    over sections of plot where the flag is True.
    """

    if flag is None: return

    if x is None: x = numpy.arange(len(flag))

    chunks = chunker(flag)
    for i, chunk in enumerate(chunks):
        ix = x[chunk[0]]; iy = x[chunk[-1]]
        if i == 0: plt.axvspan(ix, iy, label=label, **lsd)
        else: plt.axvspan(ix, iy, **lsd)

# Not often used.
def bridge_flag(flag, bs=90):
    
    flag = numpy.array(flag)
    p = flag.copy()
    q = numpy.where(p)[0]

    for i, w in enumerate(numpy.diff(q)):
        if w < bs: p[q[i]:q[i]+w] = True

    return p

def grow_flags(flag, s, one_way=False):
    
    assert(type(s) == int)

    flag = numpy.append([False]*abs(s), flag)
    flag = numpy.append(flag, [False]*abs(s))

    for i in range(abs(s)):

       if s < 0 or one_way is False:
           right_flag = numpy.roll(flag, -1)
           right_flag[-1] = False 
           flag = numpy.logical_or(flag, right_flag)
       if s > 0 or one_way is False:
           left_flag = numpy.roll(flag, 1)
           left_flag[0] = False 
           flag = numpy.logical_or(flag, left_flag)

    flag = flag[abs(s):-abs(s)]           
    return flag

# helper function to ungrow flags.
def shrink_flags(flag, s, one_way=False):
    
    assert(type(s) == int)

    for i in range(abs(s)):

       if s < 0 or one_way is False:
           right_flag = numpy.roll(flag, -1)
           right_flag[-1] = flag[-1]
           flag = numpy.logical_and(flag, right_flag)
       if s > 0 or one_way is False:
           left_flag = numpy.roll(flag, 1)
           left_flag[0] = flag[0]
           flag = numpy.logical_and(flag, left_flag)
    return flag

# VERY USEFUL: Chunks a flag array. 
def chunker(flag):
    
    split = numpy.arange(len(flag))[numpy.append(numpy.diff(flag), False)]+1
    chunks = numpy.split(numpy.arange(len(flag)), split)
    if flag[0]: return chunks[::2]
    return chunks[1::2]

def inds_split(inds):

    x = numpy.arange(len(inds))
    d = numpy.append(False, numpy.diff(inds)>1)
    return numpy.split(inds, x[d])

### DATA ANALYSIS TOOLS ###

def mean_stream(data, ws=10, flag=None):

    ws = int_check(ws)
    ws_s = ws/2; ws_e = ws/2
    if ws%2 == 1: ws_s += 1  

    if not flag is None: data[flag] = numpy.nan

    conv = numpy.ones(ws)/float(ws)
    mean = numpy.convolve(data, conv)

    return mean[ws_s:-ws_e]

def median_stream(data, ws=10, flag=None):

    ws = int_check(ws)
    ws_s = ws/2; ws_e = ws/2
    if ws%2 == 1: ws_s += 1  

    if not flag is None: data[flag] = numpy.nan

    def m_apply(i, data=data, ws_s=ws_s, ws_e=ws_e):
        chunk = max(0, i-ws_s), min(len(data)-1, i+ws_e)
        chunk = data[chunk[0]:chunk[1]]
        return numpy.nanmedian(chunk)

    median_arr = numpy.zeros(len(data))
    for i in range(len(data)): median_arr[i] = m_apply(i)

    return median_arr

# An important tool to find LOCAL outliers.
def std_stream(data, ws=20):
    
    ws = int_check(ws)
    ws_s = int(ws/2); ws_e = ws_s 
    if ws%2 == 1: ws_s += 1 

    conv = numpy.ones(ws)/float(ws)
    a = numpy.square(numpy.convolve(data, conv))
    b = numpy.convolve(numpy.square(data), conv)
    std = numpy.sqrt(b - a)
    std = std[ws_s:-ws_e]

    return std

def nan_std_stream(data, ws=20):

    ws = int_check(ws)
    ws_s = int(ws/2); ws_e = ws_s 
    if ws%2 == 1: ws_s += 1 

    conv = numpy.ones(ws)
    nancount = numpy.isnan(data).astype("float")
    nancount = numpy.convolve(nancount, conv)
    data[numpy.isnan(data)] = 0.

    a = numpy.square(numpy.convolve(data, conv)/(float(ws) - nancount))
    b = numpy.convolve(numpy.square(data), conv)/(float(ws) - nancount)
    std = numpy.sqrt(b - a)
    std[nancount > ws-1] = numpy.nan
    std = std[ws_s:-ws_e]

    return std

def cleanest_chunk(data, flag, ws=30):

    ws = int_check(ws)
    data = data.copy()

    ws_s = int(ws/2); ws_e = ws_s 
    if ws%2 == 1: ws_s += 1 

    data[flag] = numpy.nan
    std = std_stream(data , ws=ws)

    ind = numpy.nanargmin(std[ws_s+1:-ws_e-1])

    return range(ind-ws_s, ind+ws_e)

def typical_chunk(data, flag, ws=30):

    stds = []
    chunks = chunker(numpy.logical_not(flag))
    for chunk in chunks: 
        if len(chunk) < ws: continue
        stds.append(numpy.nanstd(chunk))
    if len(stds) < 3: raise RuntimeError("Not enough flag-free chunks of size %d to find a typical chunk" % ws) 
    typical_std = numpy.nanmedian(stds)

    j = numpy.argmin(numpy.abs(stds - typical_std))
    chunk = chunks[j]
    std = std_stream(data[chunk], ws=ws)
    typical_std = numpy.nanmedian(std)
    j = numpy.argmin(numpy.abs(std - typical_std))
    s = max(0, j - ws/2); e = s + ws

    return chunk[s:e]

### PROCESSING DATA / CLEANING / FIXING ###  

# Useful. Does as it's named.
def stitch_steps(data, flag, ws=0):
    
    clean = data.copy()

    chunks = chunker(flag)
    for chunk in chunks:

        ix = chunk[0]; iy = chunk[-1]
        clean[iy:] -= numpy.nanmedian(clean[iy + ws])
        clean[:ix] -= numpy.nanmedian(clean[ix - ws])
        clean[ix:iy] = 0.

    return clean

### FINDING GLITCHES, STEPS, PLATEAUS, OTHER FEATURES ###

# Rudimentary way of finding weird plateaus.
def find_plateaus(data, steppe=200, diff=10, ws=7):
    
    flag = numpy.abs(numpy.append(numpy.diff(data), 0)) < diff
    flag = numpy.logical_and(flag, data > steppe)

    plateaus = []
    for c in chunker(flag):
        if len(c) < ws: continue
        if numpy.sum(flag[c]) < ws: continue 
        plateaus.append(c)

    return plateaus        

# Creat a template with an offset and decimation. Will create a few offsets.
def step_template():

    template = numpy.array([0.]*(2400) + [9250.]*(2400))
    template = uf.butterworth_filter(template)
    template-= numpy.mean(template)
    template/= (template[-1] - template[0])
    return template

def step_finder(data, cut=-50000, decimate=120, shift=110):

    decimate = int_check(decimate)
    fine = numpy.repeat(data, decimate)
    fine[numpy.arange(len(fine))%decimate != 0] = 0

    template = step_template()
    conv = numpy.convolve(fine, template, mode='same')
    conv = mean_stream(numpy.roll(conv, -shift), ws=120)
    flag = conv < cut
    flag = grow_flags(flag, 10, one_way=False)
    flag = grow_flags(flag, 40, one_way=True)

    inds = []
    dead_vals = 0
    for chunk in chunker(flag): 
        dead_vals += len(chunk)
        inds.append(chunk[numpy.argmin(conv[chunk])])

    inds = numpy.array(inds).astype("float"); decimate = float(decimate)
    return flag[::120], inds/decimate 

# Basic glitch finder based on local outliers.
def spike_finder(data, cut=5., ws=(5, 10)):
    
    sample = data[data > 0]
    std = numpy.nanstd(sample)
    cut = -std*cut

    flag = data < cut
    flag = grow_flags(flag,-ws[0], one_way=True)
    flag = grow_flags(flag, ws[1], one_way=True)

    glch_locs = []
    chunks = chunker(flag)
    for chunk in chunks:
        
        glch = data[chunk].copy()
        if len(glch) < 2: continue
        a = numpy.nanargmin(glch) # Find the 'peak' of the glitch
        glch_locs.append(a + chunk[0])

        curr_flag = numpy.zeros(len(glch)).astype("bool")
        curr_flag[a] = True # Flag the peak
        curr_flag = grow_flags(curr_flag,-ws[0], one_way=True)
        curr_flag = grow_flags(curr_flag, ws[1], one_way=True)
        # The above code establishes a region a little different from the 
        # original flag code. This is always (1 + 2*5 + 10) samples flagged
        # in my desired pattern around the peak. The original flagging
        # was growing an unknown amount of flags that made the cut.

        while (glch[a] < cut):
           
           glch[curr_flag] = numpy.nan # cover this peak and look for others.
           if numpy.all(numpy.isnan(glch)): break
           a = numpy.nanargmin(glch) # Find the next peak worth investigating.

           # If this is just on the edge of the last region, don't bother.
           curr_flag = grow_flags(curr_flag, 2, one_way=False) 
           if curr_flag[a]: continue
           glch_locs.append(a + chunk[0])

           curr_flag = numpy.zeros(len(glch)).astype("bool")
           curr_flag[a] = True
           curr_flag = grow_flags(curr_flag,-ws[0], one_way=True)
           curr_flag = grow_flags(curr_flag, ws[1], one_way=True)
           curr_flag[numpy.isnan(glch)] = True 

    return flag, numpy.array(glch_locs)

### GENERATING FAKE DATA METHODS ###

# Fake step
def generate_step(l, index=None, offset=None, decimate=120):
        
    assert(index < l)
    
    l = int_check(l) 
    index = int_check(index)
    offset = int_check(offset) 
    decimate = int_check(decimate)

    if offset is None: offset = numpy.random.randint(241)- 120
    if index  is None: index  = numpy.random.randint(l-26)

    template = numpy.array([0.]*(2400-offset) + [9250.]*(2400+offset))
    template = uf.butterworth_filter(template)
    template = template[::decimate]
    template = numpy.append([template[0]]*index, template)
    template = numpy.append(template, [template[-1]]*l)

    m = numpy.argmax(abs(template))

    return template[m-index:m+(l-index)]

# Fake glitch
def generate_glch(l, index=None, size=None, offset=None, decimate=120):
    
    if size   is None: size   = numpy.random.randint(30000)
    if offset is None: offset = numpy.random.randint(241) - 120
    if index  is None: index  = numpy.random.randint(l-26)

    assert(index < l) 

    l = int_check(l) 
    index = int_check(index)
    offset = int_check(offset) 
    decimate = int_check(decimate)

    n = 5000 + (decimate - 5000%decimate)
    m = 2500 + (decimate - 2500%decimate)
    template = numpy.zeros(n)
    template[m+offset] = -1.
    template = uf.butterworth_filter(template)
    template/= abs(numpy.min(template))/size
    template = template[::decimate]

    m /= decimate; m = int(m)
    if m < index: template = numpy.append([0.]*(index-m), template)
    if m > index: template = template[m-index:]

    n = len(template)
    if n < l: template = numpy.append(template, [0.]*(l - n))
    if n > l: template = template[:l]

    return template

# Simple 1/f and rolloff noise spec
def noise_spec(f, plateau_amp=.5, pink_knee=.5, roll_off=10., roll_on=3.5):

    pink_coeff  = plateau_amp * pink_knee
    pink_noise = pink_coeff / (1e-3 + f)**2.
    low_pass = 1. / numpy.sqrt((1. + (f / roll_off)**2.))
    high_pass = 1. / numpy.sqrt((1. + (roll_on / f))**2.)
    return (pink_noise + plateau_amp) * low_pass * high_pass

# Reading geant4 parsed outputs.
def particle_spectrum(en, f):
    
    arr, err, bins = numpy.load(f)
    x = (bins[1:] + bins[:-1])/2.
    arr = arr[:-1]
    err = err[:-1]
    return numpy.interp(en, x, arr)

# Manipulating geant4 parsed outputs.
def inv_particle(N, f):
    
    ir = numpy.random.random(N)
    arr, err, bins = numpy.load(f)
    arr = arr[:-1]
    arr = numpy.cumsum(arr[::-1])[::-1]
    arr /= max(arr)
    x = (bins[1:] + bins[:-1])/2.

    ln = len(arr[arr == 0])
    arr = arr[:-ln+1]
    x = x[:-ln+1]

    y = numpy.interp(ir, arr[::-1], x[::-1])
    return y
