#!/bin/python
import os, sys, numpy, pickle, pandas

g = "/home/osherso2/scratch/glitch_libs/"
g = os.path.join(g, sys.argv[1])

mega_lib = None
mega_str = None 

dets = []
for r in range(33):
    for c in [4, 5, 6, 7]:
        dets.append((r, c))
N = 33; M = len(dets)/N
partial_dets = lambda i: dets[M*i:M*(i+1)]

for i in range(N):

    libf = g + "_part%02d" % i 
    strf = libf + "_SR"
    print libf

    if os.path.isfile(strf):
        try: status_report = pandas.read_pickle(strf)
        except: raise RuntimeError("corrupt status_report?")
        try: lib = pandas.read_pickle(libf)
        except: 
            lib = None
            print "missing lib (%s part %d)?" % (g, i)
    else: raise RuntimeError("not fully cooked")

    for det in partial_dets(i):
        
        r, c = det
        if "r%02dc%02d" % (r, c) not in numpy.array(status_report["det"]): raise RuntimeError("not fully cooked (r%02dc%02d)" % (r, c))

    if mega_str is None: mega_str = status_report 
    else: mega_str = mega_str.append(status_report, sort=True).reset_index(drop=True)
    if not lib is None:
        if mega_lib is None: mega_lib = lib
        else: mega_lib = mega_lib.append(lib, sort=False).reset_index(drop=True)

mega_lib.to_pickle(g)
mega_str.to_pickle(g+"_SR")

