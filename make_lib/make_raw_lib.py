import os, sys, numpy, matplotlib.pyplot as plt, pandas
sys.path.append("/home/osherso2/documents/monotile_analysis/slow_data/")
import FNDR 
from TOD_lib import chunker

import warnings
warnings.filterwarnings("ignore")

f = "/projects/physics/filippini/RUN19/20171012"
f = os.path.join(f, "run19_126Hz_DM10_SQsRebiased_allcols_20171012")

def get_events(det, f=f, cuts=[20., 15., 10., 5., 5.]):

    det, cor = FNDR.read_det(det)
    tod = FNDR.FNDR()
    tod.load_dirfile(f=f, det=det)
    tod.raw_to_data()
    tod.data_to_clean()
    tod.find_glitches(cuts=cuts)

    flx_reverb = 02200
    reverb = numpy.zeros(tod.N).astype("bool")
    for i in x[tod.usr]: reverb[i:i+flx_reverb] = True

    stats = {}
    x = numpy.arange(tod.N)
    stats["det"] = det
    stats["std"] = numpy.nanmedian(tod.stdstream)
    stats["N"] = tod.N
    stats["flx inds"] = x[tod.usr]
    stats["live time"] = 0 
    stats["reverb"] = reverb 

    try: 
        tod.make_lib()
        stats["dead"] = len(tod.lib) == 0
        stats["flag"] = tod.flag_hist[0]
        stats["live time"] = tod.N - len(x[stats["flag"]])
        stats = pandas.DataFrame([stats])
        return tod.lib, stats 
    except:
        stats["dead"] = True 
        stats["flag"] = None
        stats["live time"] = None 
        stats = pandas.DataFrame([stats])
        return None, stats 

dets = []
for r in range(33):
    for c in [4, 5, 6, 7]:
        dets.append((r, c))
N = 33; M = len(dets)/N
partial_dets = lambda i: dets[M*i:M*(i+1)]

def construct_lib(libf, i):

    print
    libf += "_part%02d" % i 
    print libf
    strf=libf+"_SR"

    if os.path.isfile(strf):
        print "resuming lib making"
        try: status_report = pandas.read_pickle(strf)
        except: raise RuntimeError("failed to load status report")
        try: lib = pandas.read_pickle(libf)
        except: raise RuntimeError("failed to load lib")
    else:
        print "creating new lib"
        status_report = None 
        lib = None

    for det in partial_dets(i):

        det, cor = FNDR.read_det((det[0], det[1]))
        print
        print det, 
        sys.stdout.flush()
        if not status_report is None:
            if det in numpy.array(status_report["det"]):
                print "resumed",
                continue

        nlib, stats = get_events(cor)

        if nlib is None:
            print "dead or empty",
            if status_report is None: status_report = stats 
            else: status_report = status_report.append(stats, sort=False).reset_index(drop=True)
            status_report.to_pickle(strf)
            continue

        if len(nlib) == 0:
            print "empty or dead",
            if status_report is None: status_report = stats 
            else: status_report = status_report.append(stats, sort=False).reset_index(drop=True)
            status_report.to_pickle(strf)
            continue
        
        print "active",
        
        if lib is None: lib = nlib
        else: lib = lib.append(nlib, sort=False).reset_index(drop=True)
        lib.to_pickle(libf)

        if status_report is None: status_report = stats 
        else: status_report = status_report.append(stats, sort=False).reset_index(drop=True)
        status_report.to_pickle(strf)

    print 
    print "done",

g = "/home/osherso2/scratch/glitch_libs/"
g = os.path.join(g, sys.argv[1])
construct_lib(g, int(sys.argv[2]))

