#!/bin/bash

f="lib_f00_v10"
start=$(date)

if [ -z $1 ]
then
	rm ./logs/part*_log 
	rm ./logs/part*_err
	for j in $(seq 0 32) 
	do
		lo="./logs/part"$j"_log"
		le="./logs/part"$j"_err"
		qsub make_raw_lib.pbs -F "$f $j" -N lib$j -o $lo -e $le
		echo "start" >> "./logs/part"$j"_log"
	done
fi

while true : 
do
	clear
	clear
	clear
	
	var=$(python ~/conviences/qstat.py)
	python ~/conviences/qstat.py

	for j in $(seq 0 32) 
	do
		tmp=$(tail "./logs/part"$j"_log" -n 1)

		if [[ $tmp =~ r ]];
		then
			echo $tmp" "
		fi
	done

	if [[ ! $var =~ [R|Q] ]];
	then
		break
	fi
	sleep 10
done
sleep 1 

python join_raw_libs.py $f
sleep 1
python treat_lib.py $f
