import numpy
import matplotlib.pyplot as plt
from matplotlib.mlab import psd

font = {'size' : 12}
from matplotlib import rc
rc('font', **font)

def SQUID_noise(f, plateau_amp=7.5e-13, pink_knee=2., roll_off=1.5e6):

	pink_coeff  = plateau_amp * pink_knee
	pink_noise = pink_coeff / (1e-3 + f)
	low_pass = 1. / numpy.sqrt((1. + (f / roll_off)**2.))
	return (pink_noise + plateau_amp) * low_pass

class noise:

	def __init__(self, spectra=SQUID_noise):

		self.spectra = spectra

	def plot_spectra(self, spectra=None, minf=1., maxf=2.5e7, fstep=10., label=None):

		if spectra is None: spectra = self.spectra
		if minf == 0: minf = 1e-3
		f = numpy.arange(minf, maxf, fstep)
		y = spectra(f)
		plt.plot(f, y, ls=':', label=label)
		plt.yscale('log')
		plt.xscale('log')
		plt.ylabel("A/sqrt(Hz)")
		plt.xlabel("Hz")
		plt.legend()
		return f, y

	def get_rms(self, maxf=2e7, fstep=2.5):

		fbins = numpy.arange(0, maxf, fstep)
		fvars = self.spectra(fbins)
		return (numpy.trapz(fvars**2., x=fbins)**.5)

	def random_data(self, N=50*(2**18), fsamp=5e7, spectra=None, rnd=True):

		if spectra is None: spectra = self.spectra

		N = int(N)
		if N % 2 != 0: N = N + 1

		nyq = fsamp / 2.
		fstep = fsamp/N

		fbins = numpy.linspace(fstep/2, nyq, N/2)
		fvars = spectra(fbins) * (fstep/2.)**.5

		if rnd: fpows = numpy.random.normal(0, fvars)
		else: fpows = fvars
		fphis = numpy.random.rand(int(N/2))*(2.*numpy.pi)
		modes = fpows * numpy.exp(1.j * fphis)
		modes = numpy.append(modes, numpy.conjugate(modes[::-1]))
		modes = numpy.append([0], modes)

		data = numpy.fft.ifft(modes)
		data*= len(data)

		time = numpy.arange(len(data))*(1./fsamp)
		return time, numpy.real(data)

	def psd(self, time, data, label=None, plot=False):

		fsamp = 1./(time[1] - time[0])
		if len(data) > 2**16:
			v, b = psd(data, NFFT=2**15, Fs=fsamp)
			if plot: plt.plot(b, v**.5, ls='steps', label=label)
		else:
			v, b = psd(data, NFFT=len(data), Fs=fsamp)
			if plot: plt.plot(b, v**.5, ls='steps', label=label)

		return b, v

	def alias(self, spectra=None, maxf=5e6, fstep=20, decimation=2, plot=True):

		if spectra is None: spectra=self.spectra

		if decimation <= 2:

			reduced_maxf = float(maxf) / decimation
			N = int(numpy.ceil(reduced_maxf / fstep))
			f = numpy.arange(fstep/1e3, maxf, fstep)

			below = (spectra(f[:N]))**2.
			above = (spectra(f[N:][::-1]))**2.

			below[-len(above):] += above		
		
			b = f[:N]
			v = below[:N]**.5

			if plot: 
				plt.plot(b, v)
				plt.yscale('log')
				plt.xscale('log')
				plt.ylabel("A/sqrt(Hz)")
				plt.xlabel("Hz")
	
			return b, v

		if decimation > 2:

			half_spectra = lambda x: (spectra(x)**2. + spectra(maxf - x)**2.)**.5
			if plot: self.plot_spectra(spectra=half_spectra, maxf=maxf/2., fstep=fstep)
			return self.alias(spectra=half_spectra, maxf=maxf/2., fstep=fstep, decimation=decimation/2.)


