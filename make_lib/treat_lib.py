#!/bin/python
import os, sys, numpy, pickle, matplotlib.pyplot as plt, pandas
sys.path.append("/home/osherso2/documents/monotile_analysis/slow_data/")

g = "/home/osherso2/scratch/glitch_libs/"
h = sys.argv[1]
lib = pandas.read_pickle(os.path.join(g, h))
lib = lib.sample(frac=1).reset_index(drop=True)
status_report = pandas.read_pickle(os.path.join(g, h+"_SR"))

libf = os.path.join(g, h+"_c00")
strf = os.path.join(g, h+"_c00_SR")

def cut_finder(lib, cut):

    cuts = []
    for k, m in lib.iterrows(): cuts.append(cut in m["cut"])
    return numpy.array(cuts)

### EVENT CUTS ###

ind = lib["step"]
lib.loc[ind, "cut"] += ":step"

ind = lib["signal to noise"] < 5
lib.loc[ind, "cut"] += ":low S/N"

ind = lib["integral energy"] < 0
lib.loc[ind, "cut"] += ":negative integral energy" 

ind = lib["template energy"] < 0
lib.loc[ind, "cut"] += ":template energy threshhold" 

ind = numpy.abs(lib["prox to prev evn"]) < 10
lib.loc[ind, "cut"] += ":close to evn" 

ind = lib["prox to next evn"] < 15
lib.loc[ind, "cut"] += ":close to evn" 

ind = numpy.abs(lib["index fuckup"]) > 2
lib.loc[ind, "cut"] += ":index fuckup" 

ind = numpy.abs(lib["local std"]) < 1
lib.loc[ind, "cut"] += ":low std"

### REVERB CUT ###

flx_reverb = 02200.
flx_length = 15140.

dets = numpy.unique(lib["det"])
paths = ["r22c07", "r17c05", "r06c05", "r00c04", "r00c05", "r00c06", "r00c07"]
ind1 = numpy.logical_not(lib["step"])
ind2 = numpy.logical_not(cut_finder(lib, "low S/N"))
ind3 = lambda det: lib["det"] == det
ind4 = numpy.abs(lib["prox to prev flx"]) < flx_reverb  
ind5 = lib["cut"] == ""

for det in dets:

    sel = status_report["det"] == det
    
    if det in paths:
        status_report.loc[sel, "dead"].iloc[0] = True
        lib.loc[ind3(det), "cut"] += ":pathological detector"

    M = float(len(lib[numpy.logical_and(ind3(det), ind5)]))
    if M == 0: status_report.loc[sel, "dead"] = True     

    if status_report.loc[sel, "std"].iloc[0] < 1:
        status_report.loc[sel, "dead"] = True
        lib.loc[ind3(det), "cut"] += ":low detector std"

    if M == 0: continue

    ind6 = numpy.logical_and(ind1, ind2) 
    ind6 = numpy.logical_and(ind4, ind6) 
    ind6 = numpy.logical_and(ind3(det), ind6) 
    verbs = len(lib[ind6])/float(M)

    # If more than twice the percentage of events are in the reverb zone as expect, this is
    # a reverbing detector and so reverb zones are just declared as dead.
    if verbs > 2.*(flx_reverb / flx_length):
        ind6 = numpy.logical_and(ind1, ind3(det))
        ind7 = numpy.abs(lib["prox to prev flx"]) < flx_reverb
        lib.loc[numpy.logical_and(ind6, ind7), "cut"] += ":reverb"

print "writing: ", libf
lib.to_pickle(libf)
print "writing: ", strf 
status_report.to_pickle(strf)
