import numpy
import mce_data
from scipy.signal import lfilter, lfilter_zi, filtfilt, decimate
from scipy.interpolate import interp1d

MCE_filter = mce_data.MCEButterworth([32295, 15915, 32568, 16188, 5, 12])
butterworth_filter = lambda x: MCE_filter.apply_filter_fir(x) / MCE_filter.gain()

def p2spline(x1, x2, y1, y2, d1, d2, debug=False):
	"""
	My writing of a spline from the points (x1, y1, y1') and (x2, y2, y2')
	Returns the function of the spline.
	"""

	a = [[(x2 - x1)**3., (x2 - x1)**2.],[3.*(x2 - x1)**2., 2.*(x2 - x1)]]
	b = [-d1*(x2 - x1) + y2 - y1, d2]
	a, b = numpy.linalg.solve(a, b)
	f = lambda x : a*(x-x1)**3. + b*(x-x1)**2. + d1*(x-x1) + y1

	if debug:
		x = numpy.arange(x1-1, x2+1, .1)
		plt.plot(x, f(x), lw=3)
		plt.plot([x1, x2], [y1, y2], 'o')
		plt.plot([x1-1, x1+1], [y1-d1, y1+d1])
		plt.plot([x2-1, x2+1], [y2-d2, y2+d2])

	return f

def spline(data, ds=10, debug=False, kind='slinear'):
	"""
	First does a boxcar filter of window size ds
	Then decimates the data by that same factor of ds
	Then returns a function representing the spline fit
	"""

	ds = int(ds)
	
	data = numpy.convolve(data, [1./ds]*ds, mode="full")
	time = numpy.arange(len(data)) - ds/2
	
	a, b = data[0], data[-1]
	c, d = time[0], time[-1]

	data = data[::ds]
	time = time[::ds]
	data = numpy.append(a, data)
	data = numpy.append(data, b)
	time = numpy.append(c, time)
	time = numpy.append(time, d)
	
	spline = interp1d(time, data, kind=kind)
	return spline

def detrend(data, order=3, big_vector=False):
	"""
	Subtracts the polynomial fit to the data.
	if big_vector, do subtraction point by point.
	"""

        x = range(0, len(data))

        model = numpy.polyfit(x, data, int(order))

	if big_vector:
	        for i, X in enumerate(x): 
			data[i] -= numpy.polyval(model, X)
	else:
		data -= numpy.polyval(model, x)

        return data

